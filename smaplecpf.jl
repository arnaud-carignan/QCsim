# Sample from rprobability distribution, using its CPF  as input --------

function samplecpf(cpf::Array, domaincpf::Array)

x=rand();
l=length(cpf);
j=int(l/2);
k=1;
while !(x>=cpf[j] && x<cpf[j+1])
  k+=1;
  if x>=cpf[j]
    j=int(j+l/2^k);
  else
    j=int(j-l/2^k);
  end
end
slope=(domaincpf[j+1]-domaincpf[j])./(cpf[j+1]-cpf[j]);
deltacpf=x-cpf[j];
return domaincpf[j]+slope.*deltacpf 

end