#computes the fidelity of a channel

function fidelity(M::Array)
	s=size(M);
	d=sqrt(s[1]);
	return (trace(M)+d)./(d.^2+d)
end	