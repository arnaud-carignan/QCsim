# This function creates the Paulis on n qubits  (4^n elements)
function pauligen( n )
  # Basic pauli matrices (1-qubit)
  Id=[1   0 ;  0  1];
  X= [0   1 ;  1  0];
  Y= [0 -im ; im  0];
  Z= [1   0 ;  0 -1];
  #   ------//-----------
  σ={Id X Y Z};
  M=Array(Array,4^n);
  for i=0:4^n-1
    v=basen(4,i,n);
    M[i+1]=σ[v[1]+1];
    for j=2:n
      M[i+1]=kron(M[i+1],σ[v[j]+1])
    end
  end
  return M
end