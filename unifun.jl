#provides the function that computes the k fidelity bound.

function unifun(fidelity::number)
	d=2;
	k=2;
	return (d+d.^2*cos(k*acos(sqrt((d+1)./d*fidelity-1./d))).^2)./(d.^2+d)
end