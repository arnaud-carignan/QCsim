function nzqb2(fidelity::Number)
  warn("The basis is not defined: generating Paulis (in nzqb2.jl).")
  function arclength(x)
          return 2*acos((x.^2-3)./(2.*x))
  end
  Juliet=sqrt(fidelity*4.*(4+1)-4);
  Romeo=0;
  delta=(4-Juliet)./100;  # 101 points, 100 bins
  # Cumulative probability function (cpf):----------
  c=0;
  domaincpf=[Juliet-1:delta:3.];
  cpf=zeros(length(domaincpf));
  for i=1:length(cpf)-1
    c=c+(arclength(domaincpf[i])+arclength(domaincpf[i+1])).*delta./2;
     cpf[i+1]=c;
  end
  cpf=cpf./c;  #Normalization step-----------------

  radius=samplecpf(cpf,domaincpf);

  theta1=(-1)^(randbool())*acos((1+Juliet^2-radius^2)/(2*Juliet));
  u1=-1.*e^(-im*theta1);

  theta2=(-1)^(randbool())*acos((radius^2-3)/(2*radius))*rand();
  u2=(Romeo-(Juliet+u1))./abs(Romeo-(Juliet+u1)).*e^(-im*theta2);

  theta3=(-1)^(randbool())*acos(abs(Juliet+u1+u2)./2);
  u3=(Romeo-(Juliet+u1+u2))./abs(Romeo-(Juliet+u1+u2)).*e^(-im*theta3);
  u4=(Romeo-(Juliet+u1+u2+u3));

  # Generating the random unitary operation:
  D=diagm([u1, u2, u3, u4]);
  V=randU(4);
  return uni2pl(V*D*V',2,2)

end

function nzqb2(fidelity::Number,basis::Array)

  function arclength(x)
          return 2*acos((x.^2-3)./(2.*x))
  end  
  Juliet=sqrt(fidelity*4.*(4+1)-4);
  Romeo=0;
  delta=(4-Juliet)./100;  # 101 points, 100 bins
  # Cumulative probability function (cpf):----------
  c=0;
  domaincpf=[Juliet-1:delta:3.];
  cpf=zeros(length(domaincpf));
  for i=1:length(cpf)-1
    c=c+(arclength(domaincpf[i])+arclength(domaincpf[i+1])).*delta./2;
     cpf[i+1]=c;
  end
  cpf=cpf./c;  #Normalization step-----------------

  radius=samplecpf(cpf,domaincpf);

  theta1=(-1)^(randbool())*acos((1+Juliet^2-radius^2)/(2*Juliet));
  u1=-1.*e^(-im*theta1);

  theta2=(-1)^(randbool())*acos((radius^2-3)/(2*radius))*rand();
  u2=(Romeo-(Juliet+u1))./abs(Romeo-(Juliet+u1)).*e^(-im*theta2);

  theta3=(-1)^(randbool())*acos(abs(Juliet+u1+u2)./2);
  u3=(Romeo-(Juliet+u1+u2))./abs(Romeo-(Juliet+u1+u2)).*e^(-im*theta3);
  u4=(Romeo-(Juliet+u1+u2+u3));

  # Generating the random unitary operation:
  D=diagm([u1, u2, u3, u4]);
  V=randU(4);
  return uni2pl(V*D*V',2,2, basis);

end











