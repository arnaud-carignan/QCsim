#plot the fidelity of the composition of 2 channels + the lower bound I derived.
ptsnum=10;
basis=pauligen(1);
fidstart=0.8;
fidinter=0.01;
fidend=0.9;
#--------------------
fid1=zeros(int(ptsnum.*((fidend-fidstart)./fidinter+1)),1);
fid2=fid1;


i=0;
for fidelity=fidstart:fidinter:fidend
	for n=1:ptsnum
		i+=1;
		fid1[i]=fidelity;
		fid2[i]=Q.fid(Q.nzqb(fid1[i],basis)*Q.nzqb(fid1[i],basis));
	end
end

plot(x=fid1,y=fid2)

