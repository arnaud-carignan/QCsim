#using Gadfly
import Q

function arclength(x)
        return 2*acos((x.^2-3)./(2.*x))
end
Juliet=3.91;
Romeo=0;
r=Juliet-1;
delta=0.0001;

# Cumulant probability function (cpf):----------
c=0;
i=1;
rs=r:delta:3.;
l=length(rs)+1;
cpf=zeros(l);
for ray in rs
    c=c+(arclength(ray-delta)+arclength(ray)).*delta./2;
    i+=1;
    cpf[i]=c;
end
cpf=cpf./c;  #Normalization step-----------------
domaincpf=[r-delta, rs];

radius=Q.samplecpf(cpf,domaincpf)

theta1=(-1)^(randbool())*acos((1+Juliet^2-radius^2)/(2*Juliet));
u1=-1.*e^(-im*theta1);

theta2=(-1)^(randbool())*acos((radius^2-3)/(2*radius))*rand();
u2=(Romeo-(Juliet+u1))./abs(Romeo-(Juliet+u1)).*e^(-im*theta2);

theta3=(-1)^(randbool())*acos(abs(Juliet+u1+u2)./2);
u3=(Romeo-(Juliet+u1+u2))./abs(Romeo-(Juliet+u1+u2)).*e^(-im*theta3);
u4=(Romeo-(Juliet+u1+u2+u3));

#posx=real([Romeo,Juliet+u1+u2+u3,Juliet+u1+u2,Juliet+u1,Juliet]);
#posy=imag([Romeo,Juliet+u1+u2+u3,Juliet+u1+u2,Juliet+u1,Juliet]);

# Generating the random unitary operation:
D=diagm([u1, u2, u3, u4]);
V=Q.randU(4);
U=Q.uni2pl(V*D*V',2,2);














