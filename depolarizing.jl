# This generates a depolarizing channel with appropriate fidelite/decay parameter
function depolarizing(n::Integer, d::Integer ; fidelity=1.::Number, decay=d.^n./(d.^n-1)*fidelity-1./(d.^n-1)::Number)
	op=decay.*eye(d^(2*n));
	op[1,1]=1;
	return op
end
