#computes the fidelity of a channel

function unitarity(M::Array)
	s=size(M);
  dim=s[1];
	return sqrt((trace(M'*M)-1)./(dim-1))
end

