# Returns a n x n unitary matrix sampled from the Haar measure.
# Copyright (C) 2004-2009 Toby Cubitt
function randU( n::Integer)
    m = (randn(n,n) + im*randn(n,n))/sqrt(2);
    (q,r)=qr(m);
    r= diagm(diag(r)./abs(diag(r)));
    return q*r
end