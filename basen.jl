#  Decompose "num" in "d" basis. (dits)
#  num = Σ v(m)⋅d^m
#  v(m) ∈ [0,1,...,d-1]
function basen( d::Integer , num::Integer )
    if num==0
      v=0;
    return v;
    else
      ditsnum=int(floor(log(d,num)+1)); # Number of required dits
      v=zeros(Integer,ditsnum,1);
      for k=ditsnum-1:-1:0   # Start with the highest power and goes down (k is the power)
              v[k+1]=fld(num,d^k);
              num=mod(num,d^k);      # Change num in order to find the next element
      end
    end
    return v
end
function basen( d::Integer , num::Integer , ditsnum::Integer )
      v=zeros(Integer,ditsnum,1);
      for k=ditsnum-1:-1:0   # Start with the highest power and goes down (k is the power)
              v[k+1]=fld(num,d^k);
              num=mod(num,d^k);      # Change num in order to find the next element
      end
    return v
end

