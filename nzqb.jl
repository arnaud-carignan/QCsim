# Creates a unitary close to identity for 1 qubit
# fidelity: fidelity of the noise map
# unitarity: unitarity of the noise map

function nzqb(fidelity::Number; unitarity=1.::Number, gen_method="depolarization"::String)
  warn("The basis is not feeded to nzqb. Pauli-basis is generated. This will cost you some efficiency.")
  basis=pauligen(1);

  p=2.*fidelity-1;
  if unitarity-p<0
      print("Unitarity's value is $(unitarity), while the decay parameter is $(p).")
      error("Unitarity cannot be smaller than the decay parameter.")
  end

  if gen_method=="convex combination"
    epsilon=(0.5*acos(0.5*(3.*p-1))); # Ray in Bloch sphere
    # Error if epsilon is negative
    if epsilon < 0
     error("Invalid fidelity value");
    end
    #Generating a unitary with proper fidelity
    r=1-2.*rand(3,1);  # Random vector
    r=epsilon.*r./norm(r); # Random unit vector (direction), times the random ray.
    M=zeros(2,2);
    for k=1:3
    M=r[k].*basis[k+1]+M;
    end
    U=expm(im.*M);
    uni=uni2pl(U,1,2, basis);

    #Generating a depolarizing channel with proper fidelity
    depol=p.*eye(4);
    depol[1,1]=1;

    λ=sqrt((unitarity.^2-p.^2)./(1-p.^2)); #convex combination factor
    pl=(1-λ).*depol+λ.*uni;
    return pl

  elseif gen_method=="aligned"
    top=min(1,3.*unitarity.^2);
    bottom=max(0,3.*unitarity.^2-2);
    qdepol=sqrt(bottom+(top-bottom).*rand());
    qrot=sqrt((3.*unitarity.^2-qdepol.^2)./2);
    θ=acos((3.*p-qdepol)./(2.*qrot));
    pl=[1 0 0 0 ; 0 qrot.*cos(θ) -qrot.*sin(θ) 0; 0 qrot.*sin(θ) qrot.*cos(θ) 0 ; 0 0 0 qdepol];

    return pl

  elseif gen_method=="depolarization"
    p=p./unitarity;
    epsilon=(0.5*acos(0.5*(3.*p-1))); # Ray in Bloch sphere
    # Error if epsilon is negative
    if epsilon < 0
     error("Invalid fidelity value");
    end
    r=1-2.*rand(3,1);  # Random vector
    r=epsilon.*r./norm(r); # Random unit vector (direction), times the random ray.
    M=zeros(2,2);
    for k=1:3
    M=r[k].*basis[k+1]+M;
    end
    U=expm(im.*M);
    pl=unitarity.*uni2pl(U,1,2,basis);
    pl[1,1]=1;
    return pl
  else
    error("No generating method corresponding to $gen_method")
  end
end

function nzqb(fidelity::Number,basis::Array; unitarity=1.::Number, gen_method="depolarization"::String)

  p=2.*fidelity-1;
  if unitarity-p<0
      print("Unitarity's value is $(unitarity), while the decay parameter is $(p).")
      error("Unitarity cannot be smaller than the decay parameter.")
  end

  if gen_method=="convex combination"
    epsilon=(0.5*acos(0.5*(3.*p-1))); # Ray in Bloch sphere
    # Error if epsilon is negative
    if epsilon < 0
     error("Invalid fidelity value");
    end
    #Generating a unitary with proper fidelity
    r=1-2.*rand(3,1);  # Random vector
    r=epsilon.*r./norm(r); # Random unit vector (direction), times the random ray.
    M=zeros(2,2);
    for k=1:3
    M=r[k].*basis[k+1]+M;
    end
    U=expm(im.*M);
    uni=uni2pl(U,1,2, basis);

    #Generating a depolarizing channel with proper fidelity
    depol=p.*eye(4);
    depol[1,1]=1;

    λ=sqrt((unitarity.^2-p.^2)./(1-p.^2)); #convex combination factor
    pl=(1-λ).*depol+λ.*uni;
    return pl

  elseif gen_method=="depolarization"
    p=p./unitarity;
    epsilon=(0.5*acos(0.5*(3.*p-1))); # Ray in Bloch sphere
    # Error if epsilon is negative
    if epsilon < 0
     error("Invalid fidelity value");
    end
    r=1-2.*rand(3,1);  # Random vector
    r=epsilon.*r./norm(r); # Random unit vector (direction), times the random ray.
    M=zeros(2,2);
    for k=1:3
    M=r[k].*basis[k+1]+M;
    end
    U=expm(im.*M);
    pl=unitarity.*uni2pl(U,1,2,basis);
    pl[1,1]=1;
    return pl
  elseif gen_method=="aligned"
    top=min(1,3.*unitarity.^2);
    bottom=max(0,3.*unitarity.^2-2);
    qdepol=sqrt(bottom+(top-bottom).*rand());
    qrot=sqrt((3.*unitarity.^2-qdepol.^2)./2);
    θ=acos((3.*p-qdepol)./(2.*qrot));
    pl=[1 0 0 0 ; 0 qrot.*cos(θ) -qrot.*sin(θ) 0; 0 qrot.*sin(θ) qrot.*cos(θ) 0 ; 0 0 0 qdepol];

    return pl
  else
    error("No generating method corresponding to $gen_method")
  end
end

