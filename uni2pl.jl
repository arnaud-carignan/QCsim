# Unitary operator to Pauli-Liouville representation
function uni2pl( U::Array , n::Integer, d::Integer)
	warn("The basis is not defined: generating Paulis (in uni2pl.jl).")
  pliou = zeros(4^n,4^n);
  basis=pauligen(n);

  for i = 1:4^n
      for j = 1:4^n
		 pliou[i,j] = real(trace(basis[i]*U*basis[j]*U')/2^n);
      end
  end
  return pliou
end

function uni2pl( U::Array , n::Integer,d::Integer, basis::Array)
  pliou = zeros(4^n,4^n);

  for i = 1:4^n
      for j = 1:4^n
		 pliou[i,j] = real(trace(basis[i]*U*basis[j]*U')/2^n);
      end
  end
  return pliou
end
