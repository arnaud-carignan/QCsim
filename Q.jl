module Q
  export basen,pauligen,randU,uni2pl,nzqb,depolarizing,fid,samplecpf,nzqb2

  include("basen.jl")
  include("pauligen.jl")
  include("randU.jl")
  include("uni2pl.jl")
  include("nzqb.jl")
  include("depolarizing.jl")
  include("fid.jl")
  include("unitarity.jl")
  include("samplecpf.jl")
  include("nzqb2.jl")

end

